﻿using System;
using System.Drawing.Imaging;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace ScreenTcpServer
{
    //屏幕辅助类
    public class ScreenHelper
    {

        //返回全屏截图的字节数组数据
        public static byte[] CaptureScreen()
        {
            //获取屏幕大小
            Rectangle bounds = Screen.GetBounds(Point.Empty);
            //创建位图
            Bitmap bitmap = new Bitmap(bounds.Width, bounds.Height);
            //创建画布
            Graphics g = Graphics.FromImage(bitmap);
            //复制屏幕到位图
            g.CopyFromScreen(Point.Empty, Point.Empty, bounds.Size);
            //创建内存流
            MemoryStream ms = new MemoryStream();
            //保存位图到内存流
            bitmap.Save(ms, ImageFormat.Png);
            //返回
            return ms.ToArray();
        }

        //返回全屏截图的字节数组数据
        public static string ScreenBase64()
        {
            //截屏数据字节数组
            byte[] bytes = CaptureScreen();
            //返回
            return Convert.ToBase64String(bytes);
        }
    }
}
