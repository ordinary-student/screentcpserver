﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net;
using System.Text;


namespace ScreenTcpServer
{
    //IP辅助类
    public class IPHelper
    {
        //本机主机名
        public static string HostName = Dns.GetHostName();
        //获取本机IP列表
        public static IPAddress[] IPAddressList = Dns.GetHostAddresses(HostName);
        //本机IP
        public static string LocalIP = IPAddressList[IPAddressList.Length - 1].ToString();


        //获取本机IP地址字符串
        public static string GetLocalIPs()
        {
            //
            StringBuilder ipstr = new StringBuilder(60);
            int i = 0;
            //遍历
            foreach (IPAddress ip in IPAddressList)
            {
                //判断是否IPV4
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    //拼接IP地址字符串
                    ipstr.Append(ip.ToString()).Append("  ");
                    i++;
                }
            }
            //返回
            return ipstr.ToString();
        }
    }
}
